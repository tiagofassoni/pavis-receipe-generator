FROM python:3.10-slim

ENV KERAS_BACKEND="torch"


COPY requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir -f https://download.pytorch.org/whl/torch_stable.html -r /app/requirements.txt

# I need to run this so pretrained weights from Resnet-50 are available in the container
RUN python -c "import torchvision.models as models; model = models.resnet50(pretrained=True)"

COPY . /app

WORKDIR /app

# RUN python run.py
CMD gunicorn run:app -b 0.0.0.0:5000 -w 1